/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elmedallista.tl;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author capri
 */
public class Controller extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        switch (1) {
            case 1:
                showLogin();
                break;
            case 2:
                System.exit(0);
                break;
        }

    }

    public void showLogin() throws Exception {

        Stage stage = new Stage();

        Parent root = FXMLLoader.load(getClass().getResource("loginUI.fxml"));
        
        Scene scene = new Scene(root);
        
        
        stage.setScene(scene);
        stage.show();
    }

    public void ejecutar() {

        Controller.launch(Controller.class);

    }
;

}
