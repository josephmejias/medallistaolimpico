/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elmedallista.bl.entities;

import java.util.ArrayList;


public class Reto {
    
    private int idReto;
    private String nombre;
    private String descripcion;
    private String[] coordenadas;
    private Actividad actividad;
    private Locacion locacion;
    private Medalla medalla;
    private ArrayList<Hito> hitos = new ArrayList<>();
    private String codigoAcceso;

    @Override
    public String toString() {
        return "Reto{" + "idReto=" + idReto + ", nombre=" + nombre + ", descripcion=" + descripcion + ", coordenadas=" + coordenadas + ", actividad=" + actividad + ", locacion=" + locacion + ", medalla=" + medalla + ", hitos=" + hitos + ", codigoAcceso=" + codigoAcceso + '}';
    }

    public Reto(int idReto, String nombre, String descripcion, String[] coordenadas, Actividad actividad, Locacion locacion, Medalla medalla, String codigoAcceso) {
        this.idReto = idReto;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.coordenadas = coordenadas;
        this.actividad = actividad;
        this.locacion = locacion;
        this.medalla = medalla;
        this.codigoAcceso = codigoAcceso;
    }

    public Reto() {
    }

    /**
     * @return the idReto
     */
    public int getIdReto() {
        return idReto;
    }

    /**
     * @param idReto the idReto to set
     */
    public void setIdReto(int idReto) {
        this.idReto = idReto;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the coordenadas
     */
    public String[] getCoordenadas() {
        return coordenadas;
    }

    /**
     * @param coordenadas the coordenadas to set
     */
    public void setCoordenadas(String[] coordenadas) {
        this.coordenadas = coordenadas;
    }

    /**
     * @return the actividad
     */
    public Actividad getActividad() {
        return actividad;
    }

    /**
     * @param actividad the actividad to set
     */
    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    /**
     * @return the locacion
     */
    public Locacion getLocacion() {
        return locacion;
    }

    /**
     * @param locacion the locacion to set
     */
    public void setLocacion(Locacion locacion) {
        this.locacion = locacion;
    }

    /**
     * @return the medalla
     */
    public Medalla getMedalla() {
        return medalla;
    }

    /**
     * @param medalla the medalla to set
     */
    public void setMedalla(Medalla medalla) {
        this.medalla = medalla;
    }

    /**
     * @return the hitos
     */
    public ArrayList<Hito> getHitos() {
        return hitos;
    }

    /**
     * @param hitos the hitos to set
     */
    public void setHitos(ArrayList<Hito> hitos) {
        this.hitos = hitos;
    }

    /**
     * @return the codigoAcceso
     */
    public String getCodigoAcceso() {
        return codigoAcceso;
    }

    /**
     * @param codigoAcceso the codigoAcceso to set
     */
    public void setCodigoAcceso(String codigoAcceso) {
        this.codigoAcceso = codigoAcceso;
    }

}
