/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elmedallista.bl.entities;

import java.time.LocalDate;
import java.time.Period;


public class Atleta extends Persona{
    
    
    private LocalDate fechaNacimiento;
    private int edad;
    private Direccion direccion;
    private String genero;

    public Atleta(LocalDate fechaNacimiento, Direccion direccion, String genero, int idPersona, String nombre, String segundoNombre, String apellidos, String cedula, Pais pais, String correo, String clave) {
        super(idPersona, nombre, segundoNombre, apellidos, cedula, pais, correo, clave);
        this.fechaNacimiento = fechaNacimiento;
        Period periodo = Period.between(fechaNacimiento, LocalDate.now());
        this.edad = periodo.getYears();
        this.direccion = direccion;
        this.genero = genero;
    }

    public Atleta() {
    }

    @Override
    public String toString() {
        return "Atleta{" + super.toString() + "fechaNacimiento=" + fechaNacimiento + ", edad=" + edad + ", direccion=" + direccion + ", genero=" + genero + '}';
    }
    
    

    /**
     * @return the fechaNacimiento
     */
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * @param fechaNacimiento the fechaNacimiento to set
     */
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the direccion
     */
    public Direccion getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the genero
     */
    public String getGenero() {
        return genero;
    }

    /**
     * @param genero the genero to set
     */
    public void setGenero(String genero) {
        this.genero = genero;
    }



}
