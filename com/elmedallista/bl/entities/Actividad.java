/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elmedallista.bl.entities;


public class Actividad {
    
    private int idActividad;
    private String codigo;
    private String nombre;
    private String rutaIcono;

    @Override
    public String toString() {
        return "Actividad{" + "idActividad=" + idActividad + ", codigo=" + codigo + ", nombre=" + nombre + ", rutaIcono=" + rutaIcono + '}';
    }

    public Actividad(int idActividad, String codigo, String nombre, String rutaIcono) {
        this.idActividad = idActividad;
        this.codigo = codigo;
        this.nombre = nombre;
        this.rutaIcono = rutaIcono;
    }

    public Actividad() {
    }

    /**
     * @return the idActividad
     */
    public int getIdActividad() {
        return idActividad;
    }

    /**
     * @param idActividad the idActividad to set
     */
    public void setIdActividad(int idActividad) {
        this.idActividad = idActividad;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the rutaIcono
     */
    public String getRutaIcono() {
        return rutaIcono;
    }

    /**
     * @param rutaIcono the rutaIcono to set
     */
    public void setRutaIcono(String rutaIcono) {
        this.rutaIcono = rutaIcono;
    }

}
