/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elmedallista.bl.entities;


public class Hito {
    
    private int idHito;
    private Reto reto;
    private int numHito;
    private String rutaImagen;
    private String descripcion;
    private String[] coordenadas;

    public Hito() {
    }

    public Hito(int idHito, Reto reto, int numHito, String rutaImagen, String descripcion, String[] coordenadas) {
        this.idHito = idHito;
        this.reto = reto;
        this.numHito = numHito;
        this.rutaImagen = rutaImagen;
        this.descripcion = descripcion;
        this.coordenadas = coordenadas;
    }

    @Override
    public String toString() {
        return "Hito{" + "idHito=" + idHito + ", reto=" + reto + ", numHito=" + numHito + ", rutaImagen=" + rutaImagen + ", descripcion=" + descripcion + ", coordenadas=" + coordenadas + '}';
    }
    
    

    /**
     * @return the idHito
     */
    public int getIdHito() {
        return idHito;
    }

    /**
     * @param idHito the idHito to set
     */
    public void setIdHito(int idHito) {
        this.idHito = idHito;
    }

    /**
     * @return the reto
     */
    public Reto getReto() {
        return reto;
    }

    /**
     * @param reto the reto to set
     */
    public void setReto(Reto reto) {
        this.reto = reto;
    }

    /**
     * @return the numHito
     */
    public int getNumHito() {
        return numHito;
    }

    /**
     * @param numHito the numHito to set
     */
    public void setNumHito(int numHito) {
        this.numHito = numHito;
    }

    /**
     * @return the rutaImagen
     */
    public String getRutaImagen() {
        return rutaImagen;
    }

    /**
     * @param rutaImagen the rutaImagen to set
     */
    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the coordenadas
     */
    public String[] getCoordenadas() {
        return coordenadas;
    }

    /**
     * @param coordenadas the coordenadas to set
     */
    public void setCoordenadas(String[] coordenadas) {
        this.coordenadas = coordenadas;
    }
    
    
}
