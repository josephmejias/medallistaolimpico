/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elmedallista.bl.entities;


public class Provincia extends Locacion {
    
    private Pais pais;

    @Override
    public String toString() {
        return "Provincia={" + super.toString() + "pais=" + pais.toString() + '}';
    }

    

    public Provincia(Pais pais, int idLocacion, String nombre, String codigo) {
        super(idLocacion, nombre, codigo);
        this.pais = pais;
    }

    public Provincia(Pais pais) {
        this.pais = pais;
    }

    /**
     * @return the pais
     */
    public Pais getPais() {
        return pais;
    }

    /**
     * @param pais the pais to set
     */
    public void setPais(Pais pais) {
        this.pais = pais;
    }

}
