/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elmedallista.bl.entities;


public abstract class Locacion {
    
    private int idLocacion;
    private String nombre; 
    private String codigo;

    @Override
    public String toString() {
        return "idLocacion=" + idLocacion + ", nombre=" + nombre + ", codigo=" + codigo + '}';
    }
    
    

    public Locacion(int idLocacion, String nombre, String codigo) {
        this.idLocacion = idLocacion;
        this.nombre = nombre;
        this.codigo = codigo;
    }

    public Locacion() {
    }

    /**
     * @return the idLocacion
     */
    public int getIdLocacion() {
        return idLocacion;
    }

    /**
     * @param idLocacion the idLocacion to set
     */
    public void setIdLocacion(int idLocacion) {
        this.idLocacion = idLocacion;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

}
