/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elmedallista.bl.entities;


public class Medalla {
    
    private int idMedalla;
    private String nombre;
    private String rutaImagen;
    private String descripcion;

    @Override
    public String toString() {
        return "Medalla{" + "idMedalla=" + idMedalla + ", nombre=" + nombre + ", rutaImagen=" + rutaImagen + ", descripcion=" + descripcion + '}';
    }

    public Medalla(int idMedalla, String nombre, String rutaImagen, String descripcion) {
        this.idMedalla = idMedalla;
        this.nombre = nombre;
        this.rutaImagen = rutaImagen;
        this.descripcion = descripcion;
    }

    public Medalla() {
    }

    /**
     * @return the idMedalla
     */
    public int getIdMedalla() {
        return idMedalla;
    }

    /**
     * @param idMedalla the idMedalla to set
     */
    public void setIdMedalla(int idMedalla) {
        this.idMedalla = idMedalla;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the rutaImagen
     */
    public String getRutaImagen() {
        return rutaImagen;
    }

    /**
     * @param rutaImagen the rutaImagen to set
     */
    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
