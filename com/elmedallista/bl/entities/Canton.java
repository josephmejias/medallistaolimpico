/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elmedallista.bl.entities;


public class Canton extends Locacion {
    
    private Provincia provincia;

    @Override
    public String toString() {
        return "Canton{" + super.toString() + "provincia=" + provincia + '}';
    }

    public Canton(Provincia provincia, int idLocacion, String nombre, String codigo) {
        super(idLocacion, nombre, codigo);
        this.provincia = provincia;
    }

    public Canton(Provincia provincia) {
        this.provincia = provincia;
    }

    /**
     * @return the provincia
     */
    public Provincia getProvincia() {
        return provincia;
    }

    /**
     * @param provincia the provincia to set
     */
    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

}
