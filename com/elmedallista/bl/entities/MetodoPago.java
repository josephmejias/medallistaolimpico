/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elmedallista.bl.entities;



public class MetodoPago {
    
    private int idMetodoPago;
    private String nombreTarjeta;
    private int numeroTarjeta; 
    private int mesExpiracion;
    private int anioExpiracion;

    public MetodoPago() {
    }

    public MetodoPago(int idMetodoPago, String nombreTarjeta, int numeroTarjeta, int mesExpiracion, int anioExpiracion) {
        this.idMetodoPago = idMetodoPago;
        this.nombreTarjeta = nombreTarjeta;
        this.numeroTarjeta = numeroTarjeta;
        this.mesExpiracion = mesExpiracion;
        this.anioExpiracion = anioExpiracion;
    }

    @Override
    public String toString() {
        return "MetodoPago{" + "idMetodoPago=" + idMetodoPago + ", nombreTarjeta=" + nombreTarjeta + ", numeroTarjeta=" + numeroTarjeta + ", mesExpiracion=" + mesExpiracion + ", anioExpiracion=" + anioExpiracion + '}';
    }
    
    

    /**
     * @return the idMetodoPago
     */
    public int getIdMetodoPago() {
        return idMetodoPago;
    }

    /**
     * @param idMetodoPago the idMetodoPago to set
     */
    public void setIdMetodoPago(int idMetodoPago) {
        this.idMetodoPago = idMetodoPago;
    }

    /**
     * @return the nombreTarjeta
     */
    public String getNombreTarjeta() {
        return nombreTarjeta;
    }

    /**
     * @param nombreTarjeta the nombreTarjeta to set
     */
    public void setNombreTarjeta(String nombreTarjeta) {
        this.nombreTarjeta = nombreTarjeta;
    }

    /**
     * @return the numeroTarjeta
     */
    public int getNumeroTarjeta() {
        return numeroTarjeta;
    }

    /**
     * @param numeroTarjeta the numeroTarjeta to set
     */
    public void setNumeroTarjeta(int numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    /**
     * @return the mesExpiracion
     */
    public int getMesExpiracion() {
        return mesExpiracion;
    }

    /**
     * @param mesExpiracion the mesExpiracion to set
     */
    public void setMesExpiracion(int mesExpiracion) {
        this.mesExpiracion = mesExpiracion;
    }

    /**
     * @return the anioExpiracion
     */
    public int getAnioExpiracion() {
        return anioExpiracion;
    }

    /**
     * @param anioExpiracion the anioExpiracion to set
     */
    public void setAnioExpiracion(int anioExpiracion) {
        this.anioExpiracion = anioExpiracion;
    }

}
