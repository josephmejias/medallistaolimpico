/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.elmedallista.bl.entities;


public class Distrito extends Locacion {
    
    private Canton canton;

    @Override
    public String toString() {
        return "Distrito{" + super.toString()+ "canton=" + canton + '}';
    }
    
    

    public Distrito(Canton canton, int idLocacion, String nombre, String codigo) {
        super(idLocacion, nombre, codigo);
        this.canton = canton;
    }

    public Distrito(Canton canton) {
        this.canton = canton;
    }

    /**
     * @return the canton
     */
    public Canton getCanton() {
        return canton;
    }

    /**
     * @param canton the canton to set
     */
    public void setCanton(Canton canton) {
        this.canton = canton;
    }

}
